'use strict';

const express = require('express')
const app = express()
app.use(express.json())

const firebase = require('firebase-admin')
firebase.initializeApp()
const db = firebase.firestore()
const crypto = require('crypto')
const mailjet = require('node-mailjet').connect(process.env.MAILJET_APIKEY, process.env.MAILJET_APISECRET)

const cors = require('cors')
const whitelist = ['https://mavrik.co', 'https://mavrik.netlify.app']
const corsOptions = {
  origin: function(origin, callback) {
    console.log('Origin is %s', origin)
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

const PORT = process.env.PORT || 8081
app.listen(PORT, () => console.log(`Listening on port ${PORT}!`))

// update global counter doc that holds info about total number of email signups every 60 seconds
let baseCount = 5324
let localCount = 0
let localConfirmedCount = 0
let updateCount = 0
let updateConfirmedCount = 0

setInterval(() => {
  updateCount = localCount
  updateConfirmedCount = localConfirmedCount
  let data = {
    totalEmails: firebase.firestore.FieldValue.increment(updateCount),
    totalConfirmedEmails: firebase.firestore.FieldValue.increment(updateConfirmedCount),
    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
  }
  db.collection('landing-page-emails').doc('counter').set(data, { merge: true }).then(result => {
    localCount -= updateCount
    localConfirmedCount -= updateConfirmedCount
    updateCount = 0
    updateConfirmedCount = 0
  }).catch(err => {
    console.error('Error updating global counter')
    console.error(err)
  })
}, 60 * 1000)

// update global deposit survey doc that holds info about despoit survey every 60 seconds
let localTotalDepositSurvey = 0
let localAlpha = 0
let localPrime = 0
let localMavrik = 0
let updateTotalDepositSurvey = 0
let updateAlpha = 0
let updatePrime = 0
let updateMavrik = 0
setInterval(() => {
  updateTotalDepositSurvey = localTotalDepositSurvey
  updateAlpha = localAlpha
  updatePrime = localPrime
  updateMavrik = localMavrik
  let data = {
    total: firebase.firestore.FieldValue.increment(updateTotalDepositSurvey),
    alpha: firebase.firestore.FieldValue.increment(updateAlpha),
    prime: firebase.firestore.FieldValue.increment(updatePrime),
    mavrik: firebase.firestore.FieldValue.increment(updateMavrik),
    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
  }
  db.collection('landing-page-emails').doc('depositSurvey').set(data, { merge: true }).then(result => {
    localTotalDepositSurvey -= updateTotalDepositSurvey
    localAlpha -= updateAlpha
    localPrime -= updatePrime
    localMavrik -= updateMavrik
    updateTotalDepositSurvey = 0
    updateAlpha = 0
    updatePrime = 0
    updateMavrik = 0
  }).catch(err => {
    console.error('Error updating deposit survey doc')
    console.error(err)
  })
}, 60 * 1000)

app.options('*', cors(corsOptions))

app.get('/hello', (req, res) => {
  res.send('Hello, world!')
})

app.post('/addContact', cors(corsOptions), async (req, res) => {
  let listId = 9962
  let email = req.body.email
  let referrerHash = req.body.referrer
  let promises = []

  // check if email already exists before proceeding
  db.collection('landing-page-emails').doc(email).get().then(emailDoc => {
    if (emailDoc.exists) {
      console.error('Email %s already added', email)
    } else {
      // add to mailjet
      console.log('Adding email %s to listId %s', email, listId)
      let mailjetReq = mailjet
        .post("contactslist", { 'version': 'v3' })
        .id(listId)
        .action("managecontact")
        .request({
          "Action": "addnoforce",
          "Email": email
        })
      promises.push(mailjetReq)

      mailjetReq.then((resp) => {
        if (resp.response.status != 201) {
          console.error('Response status not ok while adding %s with referrer %s to list %s', email, referrerHash, listId)
          console.error(JSON.stringify(resp))
        }
      }).catch((err) => {
        console.error('Error occured while adding %s to email list', email)
        console.error(err)
      })

      // add to firestore
      if (!referrerHash) {
        referrerHash = ''
      }
      console.log('Adding email %s with referrer %s to firestore', email, referrerHash)
      let emailHash = crypto.createHash('sha256').update(email).digest('hex')
      let data = {
        email: email,
        emailHash: emailHash,
        referrerHash: referrerHash,
        confirmed: false,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
        origPos: 0,
        currentPos: 0,
        numReferees: 0,
        numConfirmedReferees: 0
      }

      let firestoreReq = db.collection('landing-page-emails').doc(email).set(data, { merge: true })
      promises.push(firestoreReq)

      firestoreReq.then((result) => {
        localCount++
        // update numReferees for referrer 
        if (referrerHash) {

          let referrerUpdateReq = db.collection('landing-page-emails').where('emailHash', '==', referrerHash).get()
          promises.push(referrerUpdateReq)

          referrerUpdateReq.then((referrerDocSnapshot) => {
            let referrerDoc = referrerDocSnapshot.docs[0] //should be one and only one
            if (referrerDoc.exists) {
              console.log('Updating referrer info for %s for added email %s in firestore', referrerHash, email)
              let updateData = {
                numReferees: firebase.firestore.FieldValue.increment(1),
                updatedAt: firebase.firestore.FieldValue.serverTimestamp()
              }
              let referrerUpdateReq2 = referrerDoc.ref.update(updateData)
              promises.push(referrerUpdateReq2)

              referrerUpdateReq2.then((updateRes) => {
                // nothing to do
              }).catch((err) => {
                console.error('Error occured while updating numReferees for referrer %s after adding email %s', referrerHash, email)
                console.error(err)
              })
            }
          }).catch((err) => {
            console.error('Error occured while getting referrer info for %s after adding email %s', referrerHash, email)
            console.error(err)
          })
        }
      }).catch((err) => {
        console.error('Error occured while adding email %s to firestore with referrer %s', email, referrerHash)
        console.error(err)
      })
    }
  }).catch((err) => {
    console.error('Error occured while checking if email %s already exists in firestore. Not proceeding further', email)
    console.error(err)
  })

  // no need to wait since all the above are bg calls and user don't need to wait for their results
  res.sendStatus(200)
})

app.get('/confirmContact', cors(corsOptions), async (req, res) => {
  let listId = 9976
  let email = req.query.email
  let promises = []

  // check if email already confirmed before proceeding
  db.collection('landing-page-emails').doc(email).get().then(emailDoc => {
    let alreadyConfirmed = emailDoc.get('confirmed')
    if (alreadyConfirmed) {
      console.error('Email %s already confirmed', email)
    } else {
      // add to mailjet
      console.log('Confirming email %s by adding to listId %s', email, listId)
      let mailjetReq = mailjet
        .post("contactslist", { 'version': 'v3' })
        .id(listId)
        .action("managecontact")
        .request({
          "Action": "addnoforce",
          "Email": email
        })
      promises.push(mailjetReq)

      mailjetReq.then((resp) => {
        if (resp.response.status != 201) {
          console.error('Response status not ok while confirming email %s by adding to list %s', email, listId)
          console.error(JSON.stringify(resp))
        }
      }).catch((err) => {
        console.error('Adding confirmed email %s to listId %s failed', email, listId)
        console.error(err)
      })

      // get global counter
      let globalConfirmedCount = 0
      let globalCounterReq = db.collection('landing-page-emails').doc('counter').get()
      promises.push(globalCounterReq)

      globalCounterReq.then(counterResp => {
        globalConfirmedCount = counterResp.get('totalConfirmedEmails')
        let pos = globalConfirmedCount + localConfirmedCount + 1
        // update firestore
        console.log('Confirming email %s in firestore', email)
        let payload = {
          confirmed: true,
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          origPos: pos,
          currentPos: pos
        }
        let firestoreReq = db.collection('landing-page-emails').doc(email).set(payload, { merge: true })
        promises.push(firestoreReq)

        firestoreReq.then((result) => {
          localConfirmedCount++
          // update referrer info
          let referrerReq = db.collection('landing-page-emails').doc(email).get()
          promises.push(referrerReq)

          referrerReq.then(referrerResp => {
            let referrer = referrerResp.get('referrerHash')
            // update numConfirmedReferees for referrer 
            if (referrer) {

              let emailHashReq = db.collection('landing-page-emails').where('emailHash', '==', referrer).get()
              promises.push(emailHashReq)

              emailHashReq.then((referrerDocSnapshot) => {
                let referrerDoc = referrerDocSnapshot.docs[0] //should be one and only one
                if (referrerDoc.exists) {
                  // calculate new position for referrer
                  console.log('Updating jumps and other referrer info for %s for confirmed email %s in firestore', referrer, email)
                  let jumps = Math.floor(((globalConfirmedCount + localConfirmedCount) * 5) / 100) // 5%
                  let updateData = {
                    numConfirmedReferees: firebase.firestore.FieldValue.increment(1),
                    currentPos: firebase.firestore.FieldValue.increment(-jumps),
                    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
                  }
                  let referrerUpdateReq2 = referrerDoc.ref.update(updateData)
                  promises.push(referrerUpdateReq2)

                  referrerUpdateReq2.then((updateRes) => {
                    // nothing to do
                  }).catch((err) => {
                    console.error('Error occured while updating current position and numConfirmedReferees for referrer %s after confirming email %s', referrer, email)
                    console.error(err)
                  })
                }
              }).catch((err) => {
                console.error('Error occured while getting referrr info for %s after confirming email %s', referrer, email)
                console.error(err)
              })
            }

          }).catch((err) => {
            console.error('Error occured while getting referrer info for email %s', email)
            console.error(err)
          })

        }).catch((err) => {
          console.error('Error occured while confirming email %s in firestore', email)
          console.error(err)
        })
      }).catch((err) => {
        console.error('Error occured while getting global counter while confirming email %s', email)
        console.error(err)
      })
    }

  }).catch((err) => {
    console.error('Error occured while checking if email %s is already confirmed in firestore. Not proceeding further', email)
    console.error(err)
  })

  let referralId = crypto.createHash('sha256').update(email).digest('hex')
  res.redirect('https://mavrik.co/subscribed?user=' + referralId)

})

app.get('/getSubscribedCount', cors(corsOptions), async (req, res) => {
  console.log('Getting subscribed count')
  db.collection('landing-page-emails').doc('counter').get().then((counterDoc) => {
    let count = counterDoc.get('totalConfirmedEmails')
    res.send('' + count)
  }).catch((err) => {
    console.error('Error occured while getting total subscribed count')
    console.error(err)
  })
})

app.get('/getCounterResults', cors(corsOptions), async (req, res) => {
  console.log('Getting counter results')
  db.collection('landing-page-emails').doc('counter').get().then((counterDoc) => {
    res.send(JSON.stringify(counterDoc.data()))
  }).catch((err) => {
    console.error('Error occured while getting counter doc')
    console.error(err)
  })
})

app.get('/getDepositSurveyResults', cors(corsOptions), async (req, res) => {
  console.log('Getting deposit survey results')
  db.collection('landing-page-emails').doc('depositSurvey').get().then((surveyDoc) => {
    res.send(JSON.stringify(surveyDoc.data()))
  }).catch((err) => {
    console.error('Error occured while getting deposit doc')
    console.error(err)
  })
})

app.get('/getUserPosition', cors(corsOptions), async (req, res) => {
  let user = req.query.user
  console.log('Getting current position for user %s', user)
  db.collection('landing-page-emails').where('emailHash', '==', user).get().then((userDocSnapshot) => {
    let userDoc = userDocSnapshot.docs[0] //should be one and only one
    if (userDoc.exists) {
      let pos = userDoc.get('currentPos')
      // happens when number of subscibers are low (< 100) and a user has referred a lot of people or answered deposit questions
      if (pos < 0) {
        pos = 10 //randomly set to 10
      }
      res.send('#' + pos)
    }
  }).catch((err) => {
    console.error('Error occured while getting user position for user %s', user)
    console.error(err)
  })
})

app.get('/getJumps', cors(corsOptions), async (req, res) => {
  console.log('Getting jumps')
  let globalConfirmedCount = 0
  db.collection('landing-page-emails').doc('counter').get().then(counterResp => {
    globalConfirmedCount = counterResp.get('totalConfirmedEmails')
    let depositJumps = Math.floor(((globalConfirmedCount + localConfirmedCount + baseCount) * 10) / 100) // deposit survey answer jumps
    let shareJumps = Math.floor(((globalConfirmedCount + localConfirmedCount + baseCount) * 5) / 100) // share jumps
    let resp = {
      depositJumps: depositJumps,
      shareJumps: shareJumps
    }
    res.send(resp)
  }).catch((err) => {
    console.error('Error occured while getting global counter while getting jumps')
    console.error(err)
  })
})

app.post('/depositAnswer', cors(corsOptions), async (req, res) => {
  let depositClass = req.body.depositClass
  let depositRange = req.body.depositRange
  let user = req.body.user
  console.log('Adding deposit survey info to firestore for user %s', user)

  db.collection('landing-page-emails').where('emailHash', '==', user).get().then((userDocSnapshot) => {
    let userDoc = userDocSnapshot.docs[0] //should be one and only one
    if (userDoc.exists) {
      let email = userDoc.get('email')
      // calculate new position
      // get global counter
      let globalConfirmedCount = 0
      db.collection('landing-page-emails').doc('counter').get().then(counterResp => {
        let updateData = {
          depositClass: depositClass,
          depositRange: depositRange,
          updatedAt: firebase.firestore.FieldValue.serverTimestamp()
        }
        // check if deposit survey is being answered twice. If so, don't update current position
        if (userDoc.get('depositClass')) {
          console.log('Deposit survey already answered previously. Updating previous answer but not recalculating current pos')
        } else {
          globalConfirmedCount = counterResp.get('totalConfirmedEmails')
          console.log('Updating jumps for %s after deposit survey', email)
          let jumps = Math.floor(((globalConfirmedCount + localConfirmedCount) * 10) / 100) // 10%
          updateData.currentPos = firebase.firestore.FieldValue.increment(-jumps)

          // update local deposit survey
            localTotalDepositSurvey++;
            if (depositClass == 'Alpha') {
              localAlpha++
            } else if (depositClass == 'Prime') {
              localPrime++
            } else if (depositClass == 'Mavrik') {
              localMavrik++
            } else {
              console.err('Invalid deposit class')
            }
        }
        db.collection('landing-page-emails').doc(email).set(updateData, { merge: true }).then(result => {
            // nothing to do
        }).catch(err => {
          console.error('Error occured while storing deposit survey data %s for user %s', JSON.stringify(updateData), email)
          console.error(err)
        })
      }).catch((err) => {
        console.error('Error occured while getting global counter while calculating position after deposit q answered for user %s', email)
        console.error(err)
      })
    }
  }).catch((err) => {
    console.error('Error occured while getting user document to store deposit survey for user %s', user)
    console.error(err)
  })

  res.sendStatus(200)
})

module.exports = app